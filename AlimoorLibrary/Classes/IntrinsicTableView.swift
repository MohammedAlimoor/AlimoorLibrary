//
//  IntrinsicTableView.swift
//  zaimo‎
//
//  Created by mac on 29/12/2016.
//
//

import Foundation
import  UIKit
class IntrinsicTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    

    
    override var intrinsicContentSize: CGSize {
       return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
        
       // return .lightContent
        
    }
    
    //swift2
//    override func intrinsicContentSize() -> CGSize {
//        self.layoutIfNeeded()
//        return CGSizeMake(UIViewNoIntrinsicMetric, contentSize.height)
//    }
//    
}

class IntrinsicCollectionView: UICollectionView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
        
        // return .lightContent
        
    }
    
    //swift2
    //    override func intrinsicContentSize() -> CGSize {
    //        self.layoutIfNeeded()
    //        return CGSizeMake(UIViewNoIntrinsicMetric, contentSize.height)
    //    }
    //    
}
