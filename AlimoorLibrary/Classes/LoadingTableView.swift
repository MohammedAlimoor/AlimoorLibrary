//
//  LoadingTableView.swift
//  MyTable
//
//  Created by mac on 12/25/17.
//  Copyright © 2017 mac. All rights reserved.
//

import Foundation

open class LoadingTableView: TableViewHeightView {
    
    let loadingImage = UIImage(named: "loading.png")
    var loadingImageView: UIImageView
    
    required public init(coder aDecoder: NSCoder) {
        loadingImageView = UIImageView(image: loadingImage)
        super.init(coder: aDecoder)!
        addSubview(loadingImageView)
        adjustSizeOfLoadingIndicator()
    }
    
    func showLoadingIndicator() {
        loadingImageView.isHidden = false
        self.bringSubview(toFront: loadingImageView)
        
        startRefreshing()
    }
    
    func hideLoadingIndicator() {
        loadingImageView.isHidden = true
        
        stopRefreshing()
    }
    
    override open func reloadData() {
        super.reloadData()
        self.bringSubview(toFront: loadingImageView)
    }
    
    // MARK: private methods
    // Adjust the size so that the indicator is always in the middle of the screen
    override open func layoutSubviews() {
        super.layoutSubviews()
        adjustSizeOfLoadingIndicator()
    }
    
    private func adjustSizeOfLoadingIndicator() {
        
        #if !TARGET_INTERFACE_BUILDER
            // this code will run in the app itself

        let loadingImageSize = loadingImage?.size
        

        loadingImageView.frame = CGRect(x:frame.size.width/2 - loadingImageSize!.width/2,y: frame.size.height/2-loadingImageSize!.height/2,width: loadingImageSize!.width,height: loadingImageSize!.height)
        
        #else
            // this code will execute only in IB
        #endif
    }
    
    // Start the rotating animation
    private func startRefreshing() {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.isRemovedOnCompletion = false
        animation.toValue = M_PI * 2.0
        animation.duration = 0.8
        animation.isCumulative = true
        animation.repeatCount = Float.infinity
        loadingImageView.layer.add(animation, forKey: "rotationAnimation")
    }
    
    // Stop the rotating animation
    private func stopRefreshing() {
        loadingImageView.layer.removeAllAnimations()
    }
}
open class LoadingCollectionView: CollectionViewHeightView {
    
    let loadingImage = UIImage(named: "loading.png")
    var loadingImageView: UIImageView
    
    required public init(coder aDecoder: NSCoder) {
        loadingImageView = UIImageView(image: loadingImage)
        super.init(coder: aDecoder)!
        addSubview(loadingImageView)
        adjustSizeOfLoadingIndicator()
    }
    
    func showLoadingIndicator() {
        loadingImageView.isHidden = false
        self.bringSubview(toFront: loadingImageView)
        
        startRefreshing()
    }
    
    func hideLoadingIndicator() {
        loadingImageView.isHidden = true
        
        stopRefreshing()
    }
    
    override open func reloadData() {
        super.reloadData()
        self.bringSubview(toFront: loadingImageView)
    }
    
    // MARK: private methods
    // Adjust the size so that the indicator is always in the middle of the screen
    override open func layoutSubviews() {
        super.layoutSubviews()
        adjustSizeOfLoadingIndicator()
    }
    
    private func adjustSizeOfLoadingIndicator() {
        #if !TARGET_INTERFACE_BUILDER
            // this code will run in the app itself
        let loadingImageSize = loadingImage?.size
        
        //        CGRect
        
        //        CGRect(x: /2, y: CGFloat, width: CGFloat, height: CGFloat)
        
        loadingImageView.frame = CGRect(x:frame.size.width/2 - loadingImageSize!.width/2,y: frame.size.height/2-loadingImageSize!.height/2,width: loadingImageSize!.width,height: loadingImageSize!.height)
        #else
            // this code will execute only in IB
        #endif
    }
    
    // Start the rotating animation
    private func startRefreshing() {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.isRemovedOnCompletion = false
        animation.toValue = M_PI * 2.0
        animation.duration = 0.8
        animation.isCumulative = true
        animation.repeatCount = Float.infinity
        loadingImageView.layer.add(animation, forKey: "rotationAnimation")
    }
    
    // Stop the rotating animation
    private func stopRefreshing() {
        loadingImageView.layer.removeAllAnimations()
    }
}
