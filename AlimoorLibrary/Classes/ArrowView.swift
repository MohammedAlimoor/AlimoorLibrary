//
//  ArrowView.swift
//  TweetPic
//
//  Created by alimoor on 3/1/17.
//  Copyright © 2017 alimoor. All rights reserved.
//

import UIKit
//import Localize_Swift
 @IBDesignable
public class ArrowView: UIView {

    @IBInspectable var arrowColor: UIColor? = UIColor.black
    @IBInspectable var fillColor: UIColor? = UIColor.gray
    @IBInspectable var size: CGFloat = 5.5
    @IBInspectable var Right: Bool = true

    override public func layoutSubviews() {
   
    layoutIfNeeded()
    }
    func drawLeft(frame: CGRect = CGRect(x: 0, y: 1, width: 100, height: 200), borderSize: CGFloat = 5.5) {
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: frame.width + borderSize , y: 0))
        bezierPath.addLine(to: CGPoint(x: frame.minX + 0.08000 * frame.width, y: frame.minY + 0.50253 * frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.width + borderSize, y: frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.width + borderSize, y: 0))
        bezierPath.close()
//        UIColor.gray.setFill()
        fillColor?.setFill()

        bezierPath.fill()
//        UIColor.black.setStroke()
        arrowColor?.setStroke()

        bezierPath.lineWidth = borderSize
        bezierPath.lineCapStyle = .round
        bezierPath.lineJoinStyle = .round
        bezierPath.stroke()
    }

    
    func drawRight(frame: CGRect = CGRect(x: 0, y: 1, width: 100, height: 200), borderSize: CGFloat = 5.5) {
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x:  -borderSize, y: frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.minX + 0.89000 * frame.width, y: frame.minY + 0.48247 * frame.height))
        bezierPath.addLine(to: CGPoint(x: -borderSize, y: 0))
        bezierPath.addLine(to: CGPoint(x: -borderSize-5, y: frame.height))
        bezierPath.close()
        //UIColor.gray.setFill()
        fillColor?.setFill()
        bezierPath.fill()
        arrowColor?.setStroke()
        bezierPath.lineWidth = borderSize
        bezierPath.lineCapStyle = .round
        bezierPath.lineJoinStyle = .round
        bezierPath.stroke()
    }

    override public func draw(_ rect: CGRect) {
//        if Localize.currentLanguage() == "ar"{
//            drawLeft(frame:rect,borderSize: size)
//
//
//        }else{
//            drawRight(frame:rect,borderSize: size)
//
//        }
        if(Right){
            drawRight(frame:rect,borderSize: size)

        }else{
            drawLeft(frame:rect,borderSize: size)

        }
    }
 

}
