//
//  KeyView.swift
//  
//
//  Created by Mohammed Alimoor on 12/07/2016.
//  Copyright © 2016 Mohammed alimoor. All rights reserved.
//

import UIKit

@IBDesignable
public class KeyboardScrollView: UIScrollView {
    @IBInspectable var PaddingTop: CGFloat = 40
   @IBInspectable var OnClickClose: Bool = true

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    /// Draw the stars in interface builder
    @available(iOS 8.0, *)
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        setup()
        setNeedsDisplay()
    }
    
    
    private func setup() {        
        if(OnClickClose)
        {
            hideKeyboardWhenTappedAround()
        }
    }
    @objc func keyboardWillShow(notification:NSNotification) {
        adjustingHeight(show: true, notification: notification)

    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        adjustingHeight(show: false, notification: notification)
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        
        let userInfo = notification.userInfo!
        
        let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let changeInHeight = (keyboardFrame.height + PaddingTop) * (show ? 1 : -1)
        
        self.contentInset.bottom = changeInHeight
        
        self.scrollIndicatorInsets.bottom = changeInHeight
        
    }
    
    
    
    

    override public func willMove(toWindow newWindow: UIWindow?) {
        if (newWindow == nil) {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }

    }
    override public func didMoveToWindow() {
        if ((self.window) != nil) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        }
    

    }
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
    deinit {
//print("deinit")
    }
    
 

}
