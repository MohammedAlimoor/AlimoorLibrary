//
//  UIImageViewColor.swift
//  zaimo‎
//
//  Created by alimoor on 2/13/17.
//
//

import UIKit

extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
@IBDesignable open class UIImageViewColor: UIImageView {

    @IBInspectable var Colored: UIColor = UIColor.clear

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
//        layer.cornerRadius = layer.frame.height / 2
//        clipsToBounds = true
//        
        
        if Colored != UIColor.clear{
          self.image=self.image?.imageWithColor(color: Colored)
        }
    }
    
}

