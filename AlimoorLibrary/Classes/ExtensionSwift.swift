//
//  ExtensionSwift.swift
//  Library
//
//  Created by mac on 10/05/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation
import UIKit




extension UIView {
    
    /**
     Simply zooming in of a view: set view scale to 0 and zoom to Identity on 'duration' time interval.
     
     - parameter duration: animation duration
     */
    func zoomIn( duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform.identity
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    /**
     Simply zooming out of a view: set view scale to Identity and zoom out to 0 on 'duration' time interval.
     
     - parameter duration: animation duration
     */
    func zoomOut( duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform.identity
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    /**
     Zoom in any view with specified offset magnification.
     
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func zoomInWithEasing(duration duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (completed: Bool) -> Void in
            UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = CGAffineTransform.identity
            }, completion: { (completed: Bool) -> Void in
            })
        })
    }
    
    /**
     Zoom out any view with specified offset magnification.
     
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func zoomOutWithEasing(duration duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (completed: Bool) -> Void in
            UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            }, completion: { (completed: Bool) -> Void in
            })
        })
    }
    
}

extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as! CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 5.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}
extension UISegmentedControl{
//    func changeTitleFont(newFontName:String?, newFontSize:CGFloat?){
//        let attributedSegmentFont = NSDictionary(object: UIFont(name: newFontName!, size: newFontSize!)!, forKey: NSFontAttributeName)
//        setTitleTextAttributes(attributedSegmentFont as [NSObject : AnyObject], for: .normal)
//    }
}
@IBDesignable  public class BlurUIImageView : UIImageView
{
    @IBInspectable var Blur : Bool = false
        {
        didSet{
            if Blur == true{
                addBlurEffect()
            }
        }
    }
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    override public func layoutSubviews() {
        if Blur == true{
            addBlurEffect()
        }
    }
}



//var PkColor = UIColor(hex: "#F9E50E")
//var PdColor = UIColor(hex: "#F6E20F")
// var PDColor = UIColor(hex: "")
//@IBDesignable
//extension UIView{
//    @IBInspectable var BKColor: Bool{
//        get{
//            return BKColor
//        }
//        set{
//            if  newValue == true
//            {
//                self.backgroundColor  = PkColor
//
//            }
//        }
//    }
//    @IBInspectable var PDColor: Bool{
//        get{
//            return PDColor
//        }
//        set{
//            if  newValue == true
//            {
//                self.backgroundColor  = PdColor
//
//            }
//        }
//    }
//
//    @IBInspectable var scalertl : Bool{
//        get{
//            return scalertl
//        }
//        set{
//            if  newValue == true
//            {
//                    self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
//
//            }
//        }
//    }
//
//
//}
extension UIView{
    
    @IBInspectable var shadowOffset1: CGSize{
        get{
            return self.layer.shadowOffset
        }
        set{
            self.layer.shadowOffset = newValue
        }
    }
   
  
    
 
 
 
    
    @IBInspectable var shadowColor1: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius1: CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity1: Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
    }
    
}
extension Date {
    ///Date().toString(format:"H:mm")
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale  = Locale.init(identifier: "ar")
        return dateFormatter.string(from: self)
    }
    func toStringEn(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale  = Locale.init(identifier: "en")
        return dateFormatter.string(from: self)
    }
}


extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}
//extension UITableView {
//    var noDataLabel: UILabel {
//        let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
//        let temp = UILabel(frame: rec)
//        temp.textColor = UIColor.gray
//        temp.text = " لاتوجد بيانات لعرضها"
//        temp.textAlignment = NSTextAlignment.center
//        return temp
//    }
//    func showEmpty(){
//        self.noDataLabel.isHidden=false
//        self.self.backgroundView = self.noDataLabel
//        
//    }
//    func showEmpty(msg:String){
//        self.noDataLabel.isHidden=false
//        self.noDataLabel.text=msg
//        
//        self.self.backgroundView = self.noDataLabel
//        
//    }
//    func hideEmpty(){
//        self.noDataLabel.isHidden=true
//        //        self.self.backgroundView = self.noDataLabel
//        
//    }
//}



extension UITableView {
    var emptyview: EmptyView {
        let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        let temp = EmptyView(frame: rec)

        return temp
    }

     func  showEmpty(){
//        self.noDataLabel.isHidden=false
        self.backgroundView = self.emptyview

    }
    func  showEmptyText(text:String){
        //        self.noDataLabel.isHidden=false
            let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
            let temp = EmptyView(frame: rec)
            temp.lbl_empty.text = text
      
        self.backgroundView = temp
        
    }

     func hideEmpty(){
        for v in (self.backgroundView?.subviews)!{
        v.removeFromSuperview()
        }
    }
}

extension UICollectionView {
    var emptyview: EmptyView {
        let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        let temp = EmptyView(frame: rec)
        
        return temp
    }
    
    func  showEmpty(){
        //        self.noDataLabel.isHidden=false
        self.backgroundView = self.emptyview
        
    }
    func  showEmptyText(text:String){
        //        self.noDataLabel.isHidden=false
        let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        let temp = EmptyView(frame: rec)
        temp.lbl_empty.text = text
        
        self.backgroundView = temp
        
    }
    
    func hideEmpty(){
        for v in (self.backgroundView?.subviews)!{
            v.removeFromSuperview()
        }
    }
}

//extension UICollectionView {
//    var noDataLabel: UILabel {
//        let  rec =  CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
//        let temp = UILabel(frame: rec)
//        temp.textColor = UIColor.gray
//        temp.text = " لاتوجد بيانات لعرضها"
//        temp.textAlignment = NSTextAlignment.center
//        return temp
//    }
//    func showEmpty(){
//        self.noDataLabel.isHidden=false
//        self.self.backgroundView = self.noDataLabel
//
//    }
//
//    func hideEmpty(){
//        for v in (self.backgroundView?.subviews)!{
//            v.removeFromSuperview()
//        }        //        self.self.backgroundView = self.noDataLabel
//
//    }
//}
extension NSDictionary {
    
    var json: String {
        let invalidJson = "\"jsonConvert \" : \"Not a valid JSON\"}"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
    
}
extension UIButton {
    
    @IBInspectable var ExpandClick: Bool{
        get{
            return ExpandClick
        }
        set{
            if  newValue == true
            {
//                self.backgroundColor  = PkColor
                self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            }
        }
    }
    @IBInspectable  var localizedTitleForNormal: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .normal)
        }
        get {
            return title(for: .normal)!
        }
    }
    
    @IBInspectable  var localizedTitleForHighlighted: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .highlighted)
        }
        get {
            return title(for: .highlighted)!
        }
    }
}

extension UIBarButtonItem {
//    @IBInspectable open var localizedImage: String = ""{
//        didSet {
////            self.setNeedsDisplay()
//        }
//    }
//    
    @IBInspectable  var localizedImage: String {
        set (key) {
            self.image = UIImage(named: NSLocalizedString(key, comment: ""))
//            setTitle(NSLocalizedString(key, comment: ""), for: .normal)
        }
        get {
            return NSLocalizedString(self.localizedImage, comment: "")//title(for: .normal)!
        }
    }
//
}
//extension String {
//    
//    func sha256() -> String{
//        if let stringData = self.data(using: String.Encoding.utf8) {
//            return hexStringFromData(input: digest(input: stringData as NSData))
//        }
//        return ""
//    }
//    
//    private func digest(input : NSData) -> NSData {
//        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
//        var hash = [UInt8](repeating: 0, count: digestLength)
//        CC_SHA256(input.bytes, UInt32(input.length), &hash)
//        return NSData(bytes: hash, length: digestLength)
//    }
//    
//    private  func hexStringFromData(input: NSData) -> String {
//        var bytes = [UInt8](repeating: 0, count: input.length)
//        input.getBytes(&bytes, length: input.length)
//        
//        var hexString = ""
//        for byte in bytes {
//            hexString += String(format:"%02x", UInt8(byte))
//        }
//        
//        return hexString
//    }
//    
//}
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func dayss(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func secondss(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if dayss(from: date)    > 0 { return "\(dayss(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if secondss(from: date) > 0 { return "\(secondss(from: date))s" }
        return ""
    }
}

struct MyString {
    static func contains(_ text: String, substring: String,
                         ignoreCase: Bool = true,
                         ignoreDiacritic: Bool = true) -> Bool {
        
        var options = NSString.CompareOptions()
        
        if ignoreCase { _ = options.insert(NSString.CompareOptions.caseInsensitive) }
        if ignoreDiacritic { _ = options.insert(NSString.CompareOptions.diacriticInsensitive) }
        
        return text.range(of: substring, options: options) != nil
    }
}

extension String {
    mutating func replace(_ originalString:String, with newString:String) {
        self = self.replacingOccurrences(of: originalString, with: newString)
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
     func replace2(_ originalString:String, with newString:String) -> String {
        return self.replacingOccurrences(of: originalString, with: newString);
    }
    
}



extension UIView {
   
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}



extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension UIView {
    
    @IBInspectable var borderWidthb: CGFloat {
        get {
            return layer.borderWidth
        }
        set(newValue) {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var cornerRadiusb: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(newValue) {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderColorb: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set(newValue) {
            layer.borderColor = newValue?.cgColor
        }
    }
    
}
public class TopLableRadius: UILabel {
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners([.topLeft, .topRight], radius: 8)
    }
}
extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

extension NSMutableDictionary{
    //let text  = NSMutableDictionary(dictionary: data1!)
    //let jsonText =  text.convertDicationryToJson() as String
    
    func convertDicationryToJson( )->NSString{
        
        do {
            let jsonData = try  JSONSerialization.data(withJSONObject: self, options: [])
            
            
            
            let  theJSONText = NSString(data: jsonData,
                                        encoding: String.Encoding.utf8.rawValue)
            
              print("JSON string = \(theJSONText!)")
            
          //  theJSONText = theJSONText!.stringByReplacingOccurrencesOfString("\\", withString: "")
            return theJSONText!
            
        } catch   {
        }
        return ""
    }
    
}




extension UIRefreshControl {
    func beginRefreshingManually() {
        
                if let scrollView = superview as? UIScrollView {
                    scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
                }
        beginRefreshing()
    }
    func beginRefreshingManuallyTableView(table:UITableView) {
        table.setContentOffset(CGPoint(x: 0, y: -frame.size.height), animated: true)
//
//        if let scrollView = superview as? UIScrollView {
//            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
//        }
        beginRefreshing()
    }
    func beginRefreshingManuallyICollectionView(table:UICollectionView) {
        table.setContentOffset(CGPoint(x: 0, y: -frame.size.height), animated: true)
        //
        //        if let scrollView = superview as? UIScrollView {
        //            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        //        }
        beginRefreshing()
    }
}



//convert your html code to a regular string:
//how to use  : detailTextLabel?.text = stringText.html2String
extension String {
    
    
 
//
//    var html2AttributedString: NSAttributedString? {
//        guard
//            let data = data(using: String.Encoding.utf8)
//            else { return nil }
//        do {
//            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
//        } catch let error as NSError {
//            print(error.localizedDescription)
//            return  nil
//        }
//    }
//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
    
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern:"[A-Z0-9a-z._%+-]{2,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    //validate PhoneNumber
    var isPhoneNumberSingup: Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: "^[0-9]{10,45}$", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
        
    }
}
//-------------------------
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    convenience init(red: Int, green: Int, blue: Int,alphaa:Float) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alphaa))
    }
    convenience init(netHex:Int,alphaa:Float) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff,alphaa: alphaa)
    }
}



/*  
 to use for  rtl view 
 for  tempView:UIView in self.view.allSubViews() {
 
 self.changeViewRTL(tempView)
 }
 */
extension UIViewController : UITextFieldDelegate{
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

@available(iOS 9.0, *)
extension UIViewController {
    
    
    @IBAction func ClickDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    

    
    @available(iOS 9.0, *)

    func EnableRTLNavigationBar()  {
//        if #available(iOS 9.0, *) {
            self.navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
//        } else {
//            // Fallback on earlier versions
//        }

    }
    @available(iOS 9.0, *)
    func EnableLTRNavigationBar()  {
        self.navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboardd))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboardd() {
        view.endEditing(true)
    }
    
    
    func changeViewRTL(tempView:UIView){
        
        var constraintsToAdd: [NSLayoutConstraint] = [NSLayoutConstraint]()
        var constraintsToRemove: [NSLayoutConstraint] = [NSLayoutConstraint]()
        
        for  constraint: NSLayoutConstraint  in tempView.constraints  {
            
            
            var  firstAttribute:NSLayoutAttribute = constraint.firstAttribute
            var secondAttribute:NSLayoutAttribute = constraint.secondAttribute
            
            if (self.IsValideAttribute(attribute: firstAttribute) && self.IsValideAttribute(attribute: secondAttribute)) {
                
                firstAttribute = self.changeAttributeValue(attribute: firstAttribute);
                secondAttribute = self.changeAttributeValue(attribute: secondAttribute);
                
                constraint.constant *= -1;
                
                
                let constraintNew:NSLayoutConstraint = NSLayoutConstraint(item: constraint.firstItem, attribute: firstAttribute, relatedBy: constraint.relation , toItem: constraint.secondItem , attribute:secondAttribute, multiplier: constraint.multiplier, constant: constraint.constant);
                
                constraintsToRemove.append(constraint)
                constraintsToAdd.append(constraintNew)
                
            }
            
        }
        
        for  constraint: NSLayoutConstraint in constraintsToRemove {
            tempView.removeConstraint(constraint)
        }
        for  constraint: NSLayoutConstraint in constraintsToAdd {
            tempView.addConstraint(constraint)
        }
    }
    
    func IsValideAttribute(attribute :NSLayoutAttribute)->Bool{
        
        if  attribute == NSLayoutAttribute.leading || attribute == NSLayoutAttribute.trailing || attribute == NSLayoutAttribute.left || attribute == NSLayoutAttribute.right ||
            attribute == NSLayoutAttribute.leadingMargin || attribute == NSLayoutAttribute.trailingMargin || attribute == NSLayoutAttribute.leftMargin || attribute == NSLayoutAttribute.rightMargin {
            return true
        }else{
            return false
        }
    }
    
    func changeAttributeValue( attribute:NSLayoutAttribute) -> NSLayoutAttribute{
        var attribute = attribute
        switch (attribute) {
        case NSLayoutAttribute.leading:
            attribute = NSLayoutAttribute.trailing;
            break;
        case NSLayoutAttribute.leadingMargin:
            attribute = NSLayoutAttribute.trailingMargin;
            break;
        case NSLayoutAttribute.trailing:
            attribute = NSLayoutAttribute.leading;
            break;
        case NSLayoutAttribute.trailingMargin:
            attribute = NSLayoutAttribute.leadingMargin;
            break;
        case NSLayoutAttribute.left:
            attribute = NSLayoutAttribute.right;
            break;
        case NSLayoutAttribute.leftMargin:
            attribute = NSLayoutAttribute.rightMargin;
            break;
        case NSLayoutAttribute.right:
            attribute = NSLayoutAttribute.left;
            break;
        case NSLayoutAttribute.rightMargin:
            attribute = NSLayoutAttribute.leftMargin;
            break;
        default:
            break;
        }
        return attribute;
    }
    
}

extension UIView {
    
    func allSubViews() ->[UIView]
    {
        var arr:[UIView] = [UIView]()
        
        arr.append(self)
        
        for  subview: UIView in self.subviews 
        {
            arr.append(contentsOf: subview.allSubViews())
        }
        
        return arr;
}

}


/*
 how to use :
 view.hideByHeight(hidden: true)
 view.hideByWidth(hidden: true)
 add  constraint to view  :
 view.setConstraintConstant(constant: CGFloat(50), forAttribute: .width)
 */
//  Created by mohammed alimoor on 1/25/17.
import UIKit

extension UIView {
    func setConstraintConstant(constant: CGFloat, forAttribute attribute: NSLayoutAttribute) -> Bool {
        let constraint = self.constraintForAttribute(attribute: attribute)
        if constraint  != nil {
            constraint?.constant = constant
            return true
        } else {
            self.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: CGFloat(1.0), constant: constant))
            return false
        }
    }
    
    func constraintConstantforAttribute(attribute: NSLayoutAttribute) -> CGFloat? {
        let constraint = self.constraintForAttribute(attribute: attribute)
        if constraint != nil {
            return constraint?.constant
        } else {
            return nil
        }
    }
    
    func constraintForAttribute(attribute: NSLayoutAttribute) -> NSLayoutConstraint? {
        
        let predicate = NSPredicate(format: "firstAttribute = %d && firstItem = %@", argumentArray: [attribute, self])
        
        
        let fillteredArray =  NSArray(array: (self.superview?.constraints)!).filtered(using: predicate)
        
        
        //    var fillteredArray = self.superview?.constraints.filteredArrayUsingPredicate(predicate)
        
        // self.superview?.constraints
        if fillteredArray.count == 0 {
            return nil
        } else {
            return fillteredArray.first as? NSLayoutConstraint
        }
    }
    
    func hideView(hidden: Bool, byAttribute attribute: NSLayoutAttribute) {
        if self.isHidden != hidden {
            let constraintConstant = self.constraintConstantforAttribute(attribute: attribute)
            if hidden {
                if constraintConstant != nil {
                    self.alpha = constraintConstant!
                } else {
                    // let size = self.getSize()
                    // self.alpha =  size.height // (attribute == NSLayoutAttribute) ? size.height : size.width
                }
                self.setConstraintConstant(constant: 0, forAttribute: attribute)
                self.isHidden = true
            } else {
                if constraintConstant != nil  {
                    self.isHidden = false
                    // self.setConstraintConstant(constant: self.alpha, forAttribute: attribute)
                    self.alpha = 1
                }
            }
        }
    }
    
    func hideByHeight(hidden: Bool) {
        self.hideView(hidden: hidden, byAttribute: .height)
    }
    
    func hideByWidth(hidden: Bool) {
        self.hideView(hidden: hidden, byAttribute: .width)
    }
    
    func getSize() -> CGSize {
        self.updateSizes()
        return CGSize(width:self.bounds.size.width,height: self.bounds.size.height)
    }
    
    func sizeToSubviews() {
        self.updateSizes()
        let fittingSize = self.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        self.frame = CGRect(x:0,y: 0,width: 320,height: fittingSize.height)
    }
    
    func updateSizes() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}



fileprivate var IslamicAssociativeKey = "IslamicAssociativeKey"
public extension UIDatePicker {
    var IsIamicDate: Bool {
        get {
            
            if let activityIndicatorView = getAssociatedObject(&IslamicAssociativeKey) as? Bool {
                return activityIndicatorView
            } else {
                let activityIndicatorView = false// String()
                
                setAssociatedObject(activityIndicatorView as AnyObject, associativeKey: &IslamicAssociativeKey, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return activityIndicatorView
            }
        }
        
        set {
            setAssociatedObject(newValue as AnyObject, associativeKey:&IslamicAssociativeKey, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}


fileprivate var ActivityIndicatorViewAssociativeKey = "ActivityIndicatorViewAssociativeKey"
public extension UIView {
    
//    func progress_start(){
//        DispatchQueue.global(qos: .background).async {
//
//        DispatchQueue.main.async {
//            self.activityIndicatorView.startAnimating()
//
//            }
//
//        }
//    }
//    func progress_end(){
//        DispatchQueue.global(qos: .background).async {
//            DispatchQueue.main.async {
//                self.activityIndicatorView.stopAnimating()
//            }
//        }
//
//    }
  
    var activityIndicatorView: UIActivityIndicatorView {
        get {
            
            if let activityIndicatorView = getAssociatedObject(&ActivityIndicatorViewAssociativeKey) as? UIActivityIndicatorView {
                return activityIndicatorView
            } else {
                let activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                activityIndicatorView.activityIndicatorViewStyle = .gray
                activityIndicatorView.color = .gray
                activityIndicatorView.center = center
                activityIndicatorView.hidesWhenStopped = true
                addSubview(activityIndicatorView)
                
                setAssociatedObject(activityIndicatorView, associativeKey: &ActivityIndicatorViewAssociativeKey, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return activityIndicatorView
            }
        }
        
        set {
            addSubview(newValue)
            setAssociatedObject(newValue, associativeKey:&ActivityIndicatorViewAssociativeKey, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
public extension NSObject {
    func setAssociatedObject(_ value: AnyObject?, associativeKey: UnsafeRawPointer, policy: objc_AssociationPolicy) {
        if let valueAsAnyObject = value {
            objc_setAssociatedObject(self, associativeKey, valueAsAnyObject, policy)
        }
    }
    
    func getAssociatedObject(_ associativeKey: UnsafeRawPointer) -> Any? {
        guard let valueAsType = objc_getAssociatedObject(self, associativeKey) else {
            return nil
        }
        return valueAsType
    }
}


extension UIProgressView {
    
    @IBInspectable var barHeight : CGFloat {
        get {
            return transform.d * 2.0
        }
        set {
            // 2.0 Refers to the default height of 2
            let heightScale = newValue / 2.0
            let c = center
            transform = CGAffineTransform(scaleX: 1.0, y: heightScale)
            center = c
        }
    }
}

extension Date{
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(self)
        let latest = (earliest == now as Date) ? self : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    
    func timeAgoSinceDateAr(numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(self)
        let latest = (earliest == now as Date) ? self : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) سنة"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 سنة"
            } else {
                return "سنة"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) شهر"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 شهر"
            } else {
                return "شهر"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) اسبوع"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 اسبوع"
            } else {
                return "اسبوع"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) يوم"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 يوم"
            } else {
                return "البارحة"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) ساعة"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 ساعة"
            } else {
                return "ساعة"
            }
        } else if (components.minute! >= 2) {
            if components.minute! <= 10 {
                return "\(components.minute!) دقائق"

            }else{
                return "\(components.minute!) دقيقة"
                
            }
        } else if (components.minute! >= 1){
            if (numericDates){
                return "دقيقة"
            } else {
                return "دقيقة"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) ثانية"
        } else {
            return "الان"
        }
        
    }
}
//extension UITableView {
//    func activityIndicator(center: CGPoint = CGPoint.zero, loadingInProgress: Bool) {
//        let tag = 12093
//        if loadingInProgress {
//            var indicator = UIActivityIndicatorView()
//            
//            indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//            indicator.tag = tag
//            indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
////            indicator.color = //Your color here
//                indicator.center = center
//            indicator.startAnimating()
////            indicator.hidesWhenStopped = true
//            self.superview?.addSubview(indicator)
//        }else {
//            if let indicator = self.superview?.viewWithTag(tag) as? UIActivityIndicatorView { do {
//                indicator.stopAnimating()
//                indicator.removeFromSuperview()
//                }
//            }
//        }
//    }
//    
//}

//private var isIslamicKey: UInt8 = 0
//extension UIDatePicker{
//
//    var isIslamic :Bool { // cat is *effectively* a stored property
//        get {
//            return associatedObject(self, key: &isIslamicKey)
//            { return false} // Set the initial value of the var
//        }
//        set { associateObject(base: self, key: &isIslamicKey, value: newValue) }
//    }
////
//}




