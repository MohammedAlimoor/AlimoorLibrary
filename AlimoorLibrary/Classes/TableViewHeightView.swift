//
//  TableViewHeightView.swift
//  QD
//
//  Created by mac on 11/6/17.
//  Copyright © 2017 mac. All rights reserved.
//

import UIKit
@IBDesignable
open class TableViewHeightView: UITableView {

    @IBOutlet public weak var HeightConstraint: NSLayoutConstraint!

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        let sections: Int = self.numberOfSections
        var rows: Int = 0
        
        for i in 0..<sections {
            rows += self.numberOfRows(inSection: i)
        }
        if rows > 0 {
            if HeightConstraint != nil{
                HeightConstraint.constant = self.contentSize.height
                
            }

        }
        
    }
    

}
@IBDesignable
open class CollectionViewHeightView: UICollectionView {
    
    @IBOutlet public weak var HeightConstraint: NSLayoutConstraint!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        let sections: Int = self.numberOfSections
        var rows: Int = 0
        
        for i in 0..<sections {
            rows += self.numberOfItems(inSection: i)
        }
        if rows > 0 {
            if HeightConstraint != nil{
                HeightConstraint.constant = self.contentSize.height
                
            }
            
        }
        
    }
    
    
}
