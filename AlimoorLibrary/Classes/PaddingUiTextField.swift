//
//  PaddingLabel.swift
//  Library
//
//  Created by mac on 17/05/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

@IBDesignable public class PaddingUiTextField: UITextField {
    
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    @IBInspectable var borderBottom: Bool = false
        {
        didSet{
            if borderBottom == true{
                underlined()
            }
        }
    }
    @IBInspectable var ColorBorderBottom: UIColor = UIColor.darkGray
        {
        didSet{
            if borderBottom == true{
                underlined()
            }
        }}
    @IBInspectable var widthBorderBottom: CGFloat = 1.0
        {
        didSet{
            if borderBottom == true{
                underlined()
            }
        }}
    func underlined(){
        let border = CALayer()
        let width = CGFloat(widthBorderBottom)
        border.borderColor = ColorBorderBottom.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
   
    func setBottomBorder2() {
        let border = CALayer()
        let width = CGFloat(widthBorderBottom)
        border.borderColor = ColorBorderBottom.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height - width, width:  frame.size.width, height: frame.size.height)
        
        border.borderWidth = width
        layer.addSublayer(border)
//        layer.masksToBounds = true
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        if(borderBottom)
        {
      //  setBottomBorder()
            
        }
        let padding = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset);
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if(borderBottom)
        {
//setBottomBorder()
            
        }
        
        let padding = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset);
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        if(borderBottom)
        {
          //  setBottomBorder()
            
        }
        let padding = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset);
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    
}
@IBDesignable public class PaddingUILabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    @IBInspectable var borderBottom: Bool = false
    @IBInspectable var ColorBorderBottom: UIColor = UIColor.darkGray
    @IBInspectable var widthBorderBottom: CGFloat = 2.0
    
    
    var border:CALayer?
    
    
    
    public override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        if(borderBottom)
        {
            let border = CALayer()
            let width = CGFloat(widthBorderBottom)
            border.borderColor = ColorBorderBottom.cgColor
            border.frame = CGRect(x: 0, y: frame.size.height - width, width:  frame.size.width, height: frame.size.height)
            
            border.borderWidth = width
            layer.addSublayer(border)
            layer.masksToBounds = true
            
        }
        let padding = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset);
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    func showSelect()  {
        
        border = CALayer()
        let width = CGFloat(widthBorderBottom)
        border!.borderColor = ColorBorderBottom.cgColor
        border!.frame = CGRect(x: 0, y: frame.size.height - width, width:  frame.size.width, height: frame.size.height)
        
        border!.borderWidth = width
        layer.addSublayer(border!)
        layer.masksToBounds = true
        
        
        
        
    }
    
    func hideSelect()  {
        
        if border != nil{
            border?.removeFromSuperlayer()
            border = nil
        }
        
        
        
        
        
    }
    
    
}
