# AlimoorLibrary

[![CI Status](https://img.shields.io/travis/ameral.java@gmail.com/AlimoorLibrary.svg?style=flat)](https://travis-ci.org/ameral.java@gmail.com/AlimoorLibrary)
[![Version](https://img.shields.io/cocoapods/v/AlimoorLibrary.svg?style=flat)](https://cocoapods.org/pods/AlimoorLibrary)
[![License](https://img.shields.io/cocoapods/l/AlimoorLibrary.svg?style=flat)](https://cocoapods.org/pods/AlimoorLibrary)
[![Platform](https://img.shields.io/cocoapods/p/AlimoorLibrary.svg?style=flat)](https://cocoapods.org/pods/AlimoorLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AlimoorLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AlimoorLibrary'
```

## Author

ameral.java@gmail.com, ameral.java@gmail.com

## License

AlimoorLibrary is available under the MIT license. See the LICENSE file for more info.
